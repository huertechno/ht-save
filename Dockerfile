FROM python:3.7

#workdir /src

COPY . /tmp/code/

RUN apt-get update && apt-get upgrade -yq 

RUN cd /tmp/code/ \
    && pip install -r requeriments.txt \
    && pip install -e .


#CMD ["bash"]
CMD ["python3", "-m", "src"]
