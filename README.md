# ht-save

Aplicación que consulta por dispositivos registrados en el gateway de mozillaIot
e impacta las mediciones en una base de datos InfluxDB, para luego ser graficados
mediante Graphana

## Instrucciones para deployar

* Agregar add-on **Web-Things**

Actualmente se debe configurar primero el gateway, agregando el add-on **Web-Things**
y luego agregando la url del dispositivo. Esto debido a que aún no se tiene 
soporte para *NodeMcU*. En caso de ser un dispositivo con soporte (revisar lista en
la documentacion oficial), el add-on lo podría agregar automaticamente.

* Generar token developer

Luego, en las configuraciones se debe habilitar el token de desarrollador, el cual
se debe agregar a las configuraciones de la aplicación *ht-save*.

* Levantar solo el servicio de gateway

En este orden levantamos primero el compose solo con el servicio de mozillaIot

`
$ docker-compose up mozillaiot
`

* Finalmente levantar la app con las configuraciones

y luego de las instrucciones mencionadas, se debe agregar el token generado en las 
configuraciones de la alicacion y asi poder correr el siguiente script:

`
$ bash init-ht-save.sh
`