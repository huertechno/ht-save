'''
Se comenta sobre el programa y se debe agregar la licencia.

'''
import time
#from influxdb import InfluxDBClient
#from influxdb.client import InfluxDBClientError
from src.objects import Thing
from src.config import *


def main():
    thing = Thing(url_gateway, TOKEN_GATEWAY)
    while True:
        links_things = thing.get_link_things()
        print(thing.get_value_things(links_things))
        time.sleep(3)
    return True
    


if __name__ == "__main__":
    main()
