'''
Escribir aca sobre la licencia y el conjunto de clases según la documentación
de referencia API REST de Mozilla IoT.
'''
import requests

class Thing:
    '''
        Clase que se corresponde con las mediciones y liks de devices que entrega
        el gateway. No son los microservicios que sufre guido! xD
    '''
    properties = dict()
    token_gateway = ''
    url_gateway = ''
    links_things = []
    header = {}

    def __init__(self, url_gateway, TOKEN_GATEWAY):
        self.url_gateway = url_gateway
        self.token_gateway = TOKEN_GATEWAY
        self.header = {
            'Accept': 'application/json',
            'Authorization': self.token_gateway
            }

    def save_measure(self):
        pass


    def get_link_things(self):
        links_things = []
        try:
            url_things = '%s/things' % self.url_gateway
            r = requests.get(url_things , headers=self.header, verify=False)
            if r.status_code == 200:
                if r.json()[0]:
                    for name_proper in r.json()[0].get('properties').values():
                        links_things.append(
                            name_proper.get('links')[0].get('href')
                            )
                else:
                    print("no hay registrado things")
        except:
            print("no se pudo consultar por things")
        return links_things



    def get_value_things(self, links_things):
        reponse_things = []
        for link_thing in links_things:
            try:
                url_link_thing = self.url_gateway + link_thing
                r_property = requests.get(
                                url_link_thing,
                                headers=self.header,
                                verify=False)
                
                if r_property.status_code == 200:
                    reponse_things.append(r_property.json())
            except:
                print('error de respuesta')
        return reponse_things
