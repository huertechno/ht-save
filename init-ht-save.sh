# script para iniciar el container con la app no se requiere montar el volumen
# del codigo solo para dev.
#
# La network es la que deberia tomar por default una vez levantado MozillaIot
# desde el compose, en caso de no tomara ese nombre, se deberia inspeccionar.
#
# docker inspect mozillaiot -f "{{json .NetworkSettings.Networks }}"
#

docker run -it \
	--mount type=bind,source=$(pwd),target=/tmp/code/ \
	--network=ht-save_gateway-net \
	--name ht-save \
	ht-save
